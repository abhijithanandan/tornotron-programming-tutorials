<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php $shopTitle = 'My shop title'; ?>
    <h1> 
    <?php 
        echo $var; 
        
        //Variables  
        $var1 = "This is a string"; 
        $var2 = " This is a second string";
        //Concatenation
        $var3 = $var1 . $var2;

        //This is a single line comment
        /* This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment
        This is second line comment */

        // Math Operations
        echo 56+45;
        echo "<br>";
        echo 56-45;
        echo "<br>";
        echo 56*45;
        echo "<br>";
        echo 56/45;
        echo "<br>";
        echo 56%45;
        echo "<br>";
        //BODMAS
        echo (56+45)*100-2/5;
        echo "<br>";

        $var4 = 100;
        $var5 = 200;
        echo "The sum of var4 and var5 is " . $var4 + $var5;

        
    ?>
    
    </h1>
</body>
</html>