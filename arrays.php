<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
   <?php 
          
//Arrays --> regular arrays, associative arrays
        //Old way
        $numberList = array(1, 2, 5, 3, 6, 8);
        print_r($numberList);

        echo "<br>";
        //New way
        $numberList = [1, 2, 5, 3, 6, 8];
        print_r($numberList);

        echo "<br>";
        $mixedArray = array(34, 545, "Hello", "984", 96.9, "World");
        print_r($mixedArray);
    
        echo "<br>";
        echo "The value at position 2 of numberList array: " . $numberList[2];
        echo "<br>";
        echo "The value at position 0 of numberList array: " . $numberList[0];
        echo "<br>";
        echo "The value at position 2 of mixedArray array: " . $mixedArray[0];
        echo "<br>";
        echo "The value at position 5 of mixedArray array: " . $mixedArray[5];

        //Associative arrays

        //Old way
        $registration_form_old = array(
            "first_name" => "kevin",
            "last_name" => "powell",
            "age" => 34,
            "email_id" => "kevin.pow@gmail.com"
        );
        echo "<br>";
        print_r($registration_form_old);

        //New way
        $registration_form_new = [
            "first_name" => "kevin",
            "last_name" => "powell",
            "age" => 34,
            "email_id" => "kevin.pow@gmail.com"
        ];
        echo "<br>";
        print_r($registration_form_new);


        $registration_form = array("first_name" => "kevin", "last_name" => "powell", "age" => 34, "email_id" => "kevin.pow@gmail.com");
        $registration_form = ["first_name" => "kevin", "last_name" => "powell", "age" => 34, "email_id" => "kevin.pow@gmail.com"];

        echo "<br>";
        echo $registration_form["age"];
   
   ?> 
    <!-- Printing preformatted arrays -->
           <pre>
               <?php print_r($mixedArray); ?>
           </pre> 
   </h1>
</body>
</html>