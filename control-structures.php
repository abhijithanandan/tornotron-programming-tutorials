<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
   <h1>
        <?php 
        
            //If statement
            // $price = 10;
            // if ($price > 50) {
            //     echo "The prodcut is taxable" . "<br>";
            //     if($price > 75) {
            //         echo "Include free shipping";
            //     } else {
            //         echo "Include shipping";
            //     }

            // } else {
            //     echo "The product is not taxable";
            // }

            // echo "<br>";

            // if ($price < 20 ) {
            //     echo "Price is low";
            // } elseif ($price < 40) {
            //     echo "Price is in moderate range";
            // } elseif ($price < 60) {
            //     echo "Price is bit high";
            // } else {
            //     echo "Price is very high";
            // }

            // if ($price < 20) {
            //     echo "Price is low";
            // } else {
            //     if ($price < 40) {
            //     echo "Price is in moderate range";
            //     } else {
            //         if ($price < 60) {
            //     echo "Price is bit high";
            //         }
            //     }
            // }

            ?>
                <!-- Comparison Operators -->
                <!-- <pre>
                    equal to ==
                    identical ===
                    compare > < >= <= <>
                    not equal to !=
                    not identical !==
                </pre> -->

                <!-- Logical Operators -->
                <!-- <pre>
                    AND &&
                    OR ||
                    NOT !
                </pre> -->

                <?php

                // $sting1 = "Hello";
                // $sting2 = "65";
                // $num1 = 65;
                // if ( $sting2 == $num1) { echo "1 is ture <br>"; }
                // if ( 4 === 4) { echo "2 is ture <br>"; }
                // if ( 4 == "4") { echo "3 is ture <br>"; }
                // if ( 4 === "4") { echo "4 is ture <br>"; }
                // if ( 4 == 5) { echo "5 is ture <br>"; }

                // $price = 200;
                // $location = "Mumbai";
                // $taxable = "True";

                // if ($price > 300 && $location == "Mumbai") {
                //     echo " 1 You got an offer..yeahhhh!!!";
                // }
                // if ($price > 300 || $location == "Mumbai") {
                //     echo " 2 You got an offer..yeahhhh!!!";
                // }
                // if ($price > 300 && $location == "Mumbai" || $taxable == "False") {
                //     echo " 3 You got an offer..yeahhhh!!!";
                // }
        
                // Ture && True == True
                // True && False == False
                // False && False == False
                // False && True == False
                // True || True == True
                // True || False == True 
                // False || True == True 
                // False || False == False

                // if (4 == 4 && 5 == 5) {echo "4 == 4 && 5 == 5 True <br>";} // Ture && True
                // if (4 == 4 && 4 == 5) {echo "4 == 4 && 4 == 5 True <br>";} //True && False
                // if (4 == 5 && 5 == 5) {echo "4 == 5 && 5 == 5 True <br>";} //False && True
                // if (4 == 5 && 5 == 5) {echo "4 == 5 && 5 == 5 True <br>";} //False && False
                // if (4 == 4 || 5 == 5) {echo "4 == 4 || 5 == 5 True <br>";} // Ture || True
                // if (4 == 4 || 4 == 5) {echo "4 == 4 || 4 == 5 True <br>";} //True || False
                // if (4 == 5 || 5 == 5) {echo "4 == 5 || 5 == 5 True <br>";} //False || True
                // if (4 == 5 || 4 == 5) {echo "4 == 5 || 5 == 5 True <br>";} //False || False

            //    if (4 > 5 ) {echo "1. Yes it is true <br>";}
            //    if (4 < 5 ) {echo "2. Yes it is true <br>";}
            //    if (4 <= 4 ) {echo "3. Yes it is true <br>";}
            //    if (4 >= 4 ) {echo "4. Yes it is true <br>";}
            //    if (4 >= 5 ) {echo "5. Yes it is true <br>";}
            //    if (4 <> 5 ) {echo "6. Yes it is true <br>";}
            //    if (4 <> 4 ) {echo "7. Yes it is true <br>";}

                // if (!(4 !== 4)) { echo "2 is ture <br>"; }
                // if ( 4 != "4") { echo "3 is ture <br>"; }
                // if ( 4 !== "4") { echo "4 is ture <br>"; }
                // if ( 4 != 5) { echo "5 is ture <br>"; }

                // if (!(4 != 5)) { echo "6 is ture <br>"; }

                // $subsciption_price = 0;

                // switch($subsciption_price) {

                //     case 100:
                //         echo "Thank you for purchasing the starter package";
                //         break;
                //     case 200:
                //         echo "Thank you for purchasing the intermediate package";
                //         break;
                //     case 300:
                //         echo "Thank you for purchasing the professional package";
                //         break;
                //     case 400:
                //         echo "Thank you for purchasing the agency package";
                //         break;
                //     default:
                //         echo "Sorry your purchase is pending!!! We will get back to you.";
                //         break;
                // }

                #Loops

                // $counter = 2;
                
                // while($counter <= 100) {
                //     echo "The counter vlaue is :" . $counter . "<br>";
                //     $counter *= 3; 
                // }

                #for loop

                // for ($i = 0; $i < 100; $i++) {
                //     echo "Couter value is". $i . "<br>";
                // }

                #foreach loop

                $numbers = [654,554,545,865,456,218,147,364,985];
                // for($i=0; $i<5; $i++) {
                //     echo "The number at position " . $i . " is " . $numbers[$i] . "<br>";
                // }

                $i = 0;
                foreach($numbers as $n) {
                    echo "The number at position " . $i . " is " . $n . "<br>";
                    $i++;
                }



        ?>

   </h1> 
</body>
</html>